= FILMOTEC

Niveau : Deuxième année de BTS SIO SLAM

BARRIERE Jérémie et AUPEE Vincent

== Présentation

Mission de développement d’une application métier (exploitation d'une API) à l'aide du framework Angular.

=== Contexte

Offrir à des utilisateurs une application web leur permettant de gérer une liste de leurs films préférés. L'utilisateur de la filmothèque personnelle évaluera les films qu'il aura retenus (une évalution de 1 à 5, avec commentaire).

=== Objectif

Au cours de ce projet nous avions plusieurs objectifs :

* Interroger une API existante via la saisie d'informations dans un formulaire,
* Définir d'au moins une entité objet pour la représentation des données,
* Afficher la liste des objets, accès au détail, avec sauvegarde locale côté client
* Tests unitaires


=== Répartition des tâches

Pour ce projet, notre manière de travailler était la suivante :
Nous avons réfléchi ensemble afin de bien comprendre le projet, les tâches à effectuer et la logique à suivre.

Nous avons travailler en commun sur la comprehension et l'exploitation de l'API ainsi que sur la sauvegarde locale des données.

Ensuite Vincent s'est chargé des test unitaires, de la gestion des notes et des commentaire et Jérémie s'est occupé la gestion des favoris et de l'apparence générale de l'application.

Enfin, nous avons mit en commun notre travail afin de l'améliorer.

=== Diagramme des tiers

image::src/assets/images/3tiers.jpg[]

=== Diagramme de cas d'utilisation

image::src/assets/images/use_case.png[]

=== Diagramme des composants

image::src/assets/images/UML.png[]

== Analyse du code


=== Présentation de l'API 'The Movie Database'

The Movie Database ( www.themoviedb.org ) est une base de données populaire et modifiable par l'utilisateur pour les films et les émissions de télévision. Son API (developers.themoviedb.org) permet aux developpeurs d'utiliser la base de donnée et ses informations dans leurs applications webs et mobiles sous réserve de certaine s conditions. Une clé d'API est nécessaire pour effectuer les requêtes, cette derniere est unique à chaque développeur.




=== Exploitation de l'API TMDb

==== Recherche d'un film par son titre

Lorsque l'utilisateur arrive sur la page *home* de l'application, ce dernier découvrir une simple présentation de son fonctionnement. :

Il a alors le choix entre 2 options, *Rechercher un film* ou consulter ses *Favoris*.
Partant du principe qu'il s'agit de sa première visite sur l'application, l'utilisateur va alors selectionner la *recherche de film*.

Il va donc arrivé sur un page avec un formulaire *search-film.component.html* et pourra saisir le titre de son film préféré, par exemple *Avengers* et valider (le titre valider sera initialisé à une variable *title*):


Une liste de film dont le titre contient Avengers va alors s'afficher (La limite sur une seule page est de 20 films).

Ces informations proviennent de l'API TMDb.

Pour récuperer des informations dans l'API, le moyen utilisé est des réquetes sous la formule d'un URL. Selon les informations que l'on souhaite récuperer, la requete sera d'une construite différement.

Voici la requete que nous avons utiliser pour rechercher une liste de film par leur titre :

Voici son architecture originale : https://api.themoviedb.org/3/search/movie?api_key=<api_key>&language=fr&query=<title>&page=<numPage>

Pour facilité l'utilisation d'autres requêtes, nous avons choisi de découper notre requête en 6 parties avec que son fonctionnement soit efficace :

* 1- De quel type de requête il s'agit : `https://api.themoviedb.org/3/search/movie?` (Une requête *search/movie*)

* 2- On fait suivre la clé d'API : `?api_key=<api_key>`

* 3- On choisi la langue de rendu : `&language=fr&query=`

* 4- Le titre que l'on a écrit dans le formulaire: `<title>`

* 5- La page que l'on souhaite affiché : `&page=`

* 6- Enfin le numéro de cette dernière: `<numPage>`

* <remplacé par une chaine de caractère ou un nombre>

Dans notre cas, ces "morceaux" de requête sont chacun stocké dans une variable nommé de façon les identifier facilement dans le fichier *environments.ts*.


* 1- *searchMoviesbyTitle*
[,java]
----
  searchMoviesbyTitle: 'https://api.themoviedb.org/3/search/movie',
----
* 2- *API_TOKEN*
[,java]
----
   API_TOKEN: '?api_key=XXXXXXX',
----
* 3- *setLanguageToFrenchQuery*
[,java]
----
  setLanguageToFrenchQuery: '&language=fr&query=',
----
* 4- *title* (Cette variable est récuperé dans le formulaire)

* 5- *setPageNumber*
[,java]
----
  setPageNumber: '&page=',
----
* 6- *numPage* (Cette variable est par défaut initialisé à 1 mais peut changé grâce aux boutons de gestion de la pagination)

Lorsque l'on valider la recherche, c'est la méthode *recherche()* qui s'execute dans *search-film.component.ts*:

[,html]
----
<form [formGroup]="identForm" (ngSubmit)="recherche()"> <1>
    <div class="field">
    <label class="label">Titre</label>
    <div class="control">
    <input class="input is-success" formControlName="title" type="text" placeholder="Avengers..." value=""> <2>
    </div>
    </div>
    <div class="field is-grouped">
        <div class="control">
        <button class="button is-link">Soumettre</button>
        </div>
        </div>
</form>
----

<1> *identForm* est le nom de réference de notre formulaire
<2> *title* est la variable qui contient le titre écrit par l'utilisateur

[,java]
----
//search-film.component.ts
recherche(){
    this.FilmService.sendGetMoviesListRequest(this.identForm.value.title, this.numPage).subscribe((data: Object)=> {<1>
      this.FilmService.addInfoFilm(data['results']); <2>
      this.numPage = data['page']; <3>
      this.nbPages = data['total_pages']; <4>
      this.lesFilms = this.FilmService.getAllInfoFilm(); <5>
    });
  }
----

<1> on appelle la methode *sendGetMoviesListRequest()* de notre *FilmService* à qui on passe en paramètre la valeur de notre variable *title* et la valeur de la variable *numPage* et qui envoie la requête à l'API.

<2> on appelle la méthode *addInfoFilm()* de *FilmService* qui recupere de l'API les informations de l'Object *results* dans notre Object global *data*

<3> On initalise la valeur du nombre *page* dans la variable *numPage*

<4> On initialise la valeur du nombre *total_pages* dans la variable *ngPages*

<5> On appelle la méthode *getAllInfoFilm()* de *FilmService* qui retourne les informations récuperées et les mets dans le tableau *lesFilms*

Pour comprendre pleinement le fonctionnement de cette méthode, il faut aller dans le fichier *film.service.ts* pour voir la fonction de chaque méthode.

[,java]
----
// film.service.ts
addInfoFilm(Film: Film) {
    this.infosFilm = Film; <1>
  }

getAllInfoFilm() {
    return this.infosFilm; <2>
  }

  public sendGetMoviesListRequest(title: string, numPage: number) { <3>
    return this.http.get(environment.searchMoviesbyTitle + environment.API_TOKEN + environment.setLanguageToFrenchQuery + title + environment.setPageNumber + numPage); <4>
  }
----

<1> Le tableau *infosFilm* prend la valeur de l'Objet Film

<2> La méthode retourne la valeur du tableau *infosFilm*

<3> On passe en paramètre à la méthode la variable *title* récupéré par le formulaire et le numéro de la page *numPage*

<4> On retourne la requête composé des différentes variables issu du ficher *environment.ts* évoqué précedement.


Notre Objet *Film* provient de l'instanciation de la class *Film* ou l'on type les différentes variables provenant de l'API que l'on utilise.

[,java]
----
//film.ts
export class Film {
        public id: number;
        public title: string;
        public overview: string;
        public release_date: string;
        public poster_path: string;
        public homepage: string;
        public budget: number;
         }

----

Voici donc le résultat sur l'application :

image::src/assets/images/home.png[]

image::src/assets/images/Saisiefilm.png[]

Et le code HTML de la page *search-film.component.html* pour l'affichage de chaque film :

[,html]
----
// search-film.component.html
<div class="columns  is-multiline is-centered mt-2">
    <div class="column is-5 has-background-light mt-5 mr-2 ml-2 p-5" *ngFor="let film of lesFilms; let i =index">
        <a [routerLink]="['/film/', film.id ]" class=" has-text-dark">
            <article class="media">
                <figure class="media-left ">
                    <p><img src="https://image.tmdb.org/t/p/w500{{ film.poster_path }} " width="150"></p>
                </figure>
                <div class="media-content">
                    <div class="content">

                        <p class="has-text-weight-bold has-text-centered is-size-4">{{ film.title }}</p>

                        <p class="has-text-centered" style="margin-top: -25px;"> {{ film.release_date }}</p>

                    <p class="has-text-justified"> {{ film.overview | slice:0:200}}...</p>
                    </div>
                </div>
            </article>
        </a>
    </div>
  </div>
----

==== Selectionner un film pour accèder à plus d'informations

L'utilisateur clique alors sur un film pour accèder à ses détails.

Lorsque la page se charge, la méthode *ngOnInit()* s'execute, s'en suit alors différentes actions du point de vue du système :

[,java]
----
//film.component.ts
ngOnInit(): void {

this.activatedRoute.params.subscribe(params => { <1>
        const idURL: number= params['id'];

        this.FilmService.sendGetInformationbyIDRequest(idURL).subscribe((data: Object) => { <2>

          this.FilmService.addInfoFilm(Object(data)); <3>
          this.lesFilms = this.FilmService.getAllInfoFilm(); <4>
          this.lesFavoris = this.FilmService.getAllFavoris(); <5>
          for(let i = 0;  i < this.lesFavoris.length ; i++){ <6>
            if(this.lesFavoris[i].id == Object(data).id){
              this.showFavoriteButton = false;
            }
          }

        });
        this.FilmService.sendGetCreditsRequest(idURL).subscribe((data: Object) => { <7>
          this.FilmService.addInfoFilm(data['crew']); <8>
          this.crewCredits = this.FilmService.getAllInfoFilm(); <9>
          this.FilmService.addInfoFilm(data['cast']); <10>
          this.castCredits = this.FilmService.getAllInfoFilm(); <10>
          for( let i = 0; i< this.crewCredits.length; i++){ <11>
            if(this.crewCredits[i].job == 'Director'){
              this.directorArray.push(this.crewCredits[i].name);
            }
          }
          for( let i = 0; i< 5 ; i++){ <12>
              this.actorsArray.push(this.castCredits[i].name);
          }
        });
      });
      });
  }
----

<1> On utilise le module *activatedRoute* importer au préalable qui permet de passer le paramètre *id* dans l'URL.

<2> La requête *sendGetInformationbyIDRequest(idURL)* est une requête qui utilise l'id du film sélectionner pour récuperer des informations supplémentaires.

<3> Comme précedemment on utilse la méthode *addInfoFilm()*

<4> On appelle la méthode *getAllInfoFilm()* de *FilmService* qui retourne les informations récuperées et les mets dans la variable *lesFilms* de type *Film*

<5> Ici on récupère la liste des films favoris dans le tableau *favoris*

<6> On parcourt le tableau *favoris* pour vérifie si le film est déjà dans les favoris pour afficher ou non le bouton permettant de l'y ajouter.

<7> La requête *sendGetCreditsRequest(idURL)* qui utilise l'id du film sélectionner pour récuperer les informations consernant les acteurs et les équipes techniques du film.

<8> On recupère les infos du *crew* grâce à la méthode *addInfoFilm(data['crew'])*

<9> On  mets ces informations dans le tableau *crewCredits*

<10> Même chose que pour le *crew* mais pour tout le *cast* dans un tableau *castCredits*

<11> On boucle pour ajouter au tableau *directorArray* le/les personnes dans le travail est *réalisateur*

<12> Même chose mais pour mettre les 5 premiers acteurs du film dans le tableau *actorsArray*


[,typescrit]
----
//film.service.ts
public sendGetInformationbyIDRequest(idFilm: number) { <1>
    return this.http.get(environment.searchMovieInformationbyID + idFilm + environment.API_TOKEN + environment.setLanguageToFrench)
  }

getAllFavoris() { <2>
    this.favoris = JSON.parse(localStorage.getItem('Favoris'));
    return this.favoris
  }

public sendGetCreditsRequest(idFilm: number) { <3>
    return this.http.get(environment.searchMovieInformationbyID + idFilm + environment.searchMovieCreditsbyID + environment.API_TOKEN + environment.setLanguageToFrench)
  }
----

<1> Cette requête permet de récuperer les informations du film grâce à son idée (la différence avec la premiere requête est qu'on utilise son id et non son titre car elle permet de récuperer d'avantage d'informations)

<2> Cette méthode retourne le tableau des favoris qui est stocké dans le local storage par le nom *Favoris* sous la forme d'un objet JSON.

<3> Cette requête permet de récuperer les informations du *casting* et du *crew* du film grâce à son *id*.

Voici le résultat depuis l'application :

image::src/assets/images/filmpage.png[]

Et du point de vue HTML :

[,html]
----
// film.component.html

<div class="media-content">
                    <div class="content">
                        <p class="has-text-centered">
                            <strong class=" is-size-1	">{{ lesFilms.title }}</strong> <br> <1>
                            <strong> Sorti le : {{ lesFilms.release_date }} </strong>
                        </p>
                        <div class="has-text-justified ml-2 mr-2">
                            <p class="mb-2">
                                <strong> Réalisateur : </strong>
                            </p>
                            <div *ngFor="let item of directorArray;let i = index">
                                <p>
                                    {{ directorArray[i]}} <2>
                                </p>
                            </div>
                            <br>

                            <p class="mb-2">
                                <strong> Acteurs principaux : </strong>
                            </p>
                            <div *ngFor="let item of actorsArray;let i = index">
                                <p>
                                    {{ actorsArray[i] }} <3>
                                </p>
                            </div> <br>

                            <p class="mb-2">
                                <strong> Synopsis : </strong>
                                {{ lesFilms.overview }} <br>
                            </p>
                            <p class="mb-2">
                                <strong> Budget : </strong>
                                {{ lesFilms.budget}} $ <br>
                            </p>

                            <p class="mb-2">
                                <strong> Page officielle : </strong>
                                <a href="{{ lesFilms.homepage }}"> {{ lesFilms.homepage }} </a><br>
                            </p>
                            <p class="buttons">

                                <button class="button is-success" (click)="onClicFavoris(lesFilms.id)" <4>
                                    *ngIf="showFavoriteButton == true">
                                    <span class="icon is-small">
                                        <i class="far fa-check-square"></i>
                                    </span>
                                    <span>Ajouter aux favoris</span>
                                </button>
                            </p>
----

<1> *lesFilms* est de type Film donc il suffit d'écrire *lesFilms.<uneVariableDeLaClasse>* pour afficher l'information souhaité.

<2> On parcourt le tableau *directorArray*.

<3> On parcourt le tableau *actorsArray*.

<4> On appelle la méthode *onClicFavoris(lesFilms.id)* avec en paramètre l'id du film. Cette méthode permet d'ajouter le film a la liste des favoris. Le bouton pour ajouter le film au favoris ne s'affiche si le film n'est pas déjà dans les favoris.

==== Ajouter un film a ses favoris

Etudions maintenant le fonctionnement de la méthode *onClicFavoris()*

[,typescript]
----
//film.component.ts
onClicFavoris(idURL: number){

          this.FilmService.sendGetInformationbyIDRequest(idURL).subscribe((data: Object) => { <1>
            this.lesFavoris = this.FilmService.getAllFavoris(); <2>

              for(let i = 0;  i < this.lesFavoris.length ; i++){ <3>
                if(this.lesFavoris[i].title == Object(data).title){
                  this.duplicate++;
                }
              }
              if(this.duplicate == 0){ <4>
                this.FilmService.addFavoris(Object(data));
                this.showFavoriteButton = false;
              }else{
                this.showFavoriteButton = false;
              }

          });
      }

----

<1> On utilise la requête *sendGetInformationbyIDRequest(idURL)*

<2> On mets dans le tableau *lesFavoris* la liste de tout les favoris grâce à la méthode *getAllFavoris()*

<3> On boucle pour vérifie si le film est déjà dans les favoris ou non.

<4> S'il ne l'est pas, on l'y ajoute et on fait disparaitre le bouton, sinon le bouton disparait simplement.


[,typescript]
----
//film.service.ts
addFavoris(Film: Film) { <1>
    this.favoris = localStorage.getItem('Favoris') ? JSON.parse(localStorage.getItem('Favoris')) : [];
    this.favoris.push(Film);
    localStorage.setItem('Favoris', JSON.stringify(this.favoris));
  }

getAllFavoris() { <2>
    this.favoris = JSON.parse(localStorage.getItem('Favoris'));
    return this.favoris
}
----

<1> La méthode récupere la liste des Favoris du Local Storage si il en existe une, sinon il en créer une et y ajoute le film.

<2> La méthode mets dans le tableau *favoris* le contenu de l'item *Favoris* du local storage.

Voici le rendu sur la page :

image::src/assets/images/filmisfav.png[]

==== Noter et commenter le film

On peut remarquer qu'un bouton permettant d'ajouter une note et un commentaire au film apparaît.

image::src/assets/images/notecommentaire.png[]

Lorsque l'on écrit la note et le commentaire et que l'on appuie sur le bouton *ajouter*, la méthode *onSubmit(idFilm: number)*

[,typescript]
----
//film.component.ts
onSubmit(idFilm: number){
        this.FilmService.addNoteInLocalStorage({idFilm: idFilm, comment: this.noteForm.value.comment, noteStar: this.noteForm.value.noteStar});

        this.showCommentForm = false;
        this.showCommentButton = false;
      }
----

La fonction ajoute l'id du film, la note et le commentaire inscrit dans le local storage grâce à la méthode *addNoteInLocalStorage()* qui prend en paramètre les 3 elements précedemments cités.

De plus on trouve dans le *ngOnInit* une condition pour limité le nombre de note et commentaire d'un film à 1 et une ligne pour la récupération de l'item dans le local storage.

[,typescript]
----
//film.component.ts
this.notes = this.FilmService.getNotesInLocalStoage();
        this.noteForm=this.formBuilder.group({
          comment: new FormControl(""),
          noteStar: new FormControl("")
      });

if(this.notes.length != 0){
          for(let i = 0; i < this.notes.length; i++){

            if(this.notes[i].idFilm == idURL){
              this.showCommentForm = false;
              this.showCommentButton = false;
            }
          }


        }
----

[,typescript]
----
export type Note = {
  idFilm: number;
  comment: string;
  noteStar: number;
}

private notes: Note[] = [];

addNoteInLocalStorage(note: Note): void {
    this.notes.push(note);
    localStorage.setItem('notes', JSON.stringify(this.notes));
  }

  getNotesInLocalStoage(): Note[] {
    this.notes = JSON.parse(localStorage.getItem('notes') || '[]');
    return this.notes;
  }
----

Les 2 méthodes fonctionnent comme *addFavoris()* et *getAllFavoris()* mais ici pour la note et le commentaire dans un type *Note*.


image::src/assets/images/note_commentaire.png[]

Du point de vu de l'HTML :

[,html]
----
//film.component.html
    <div *ngIf="showCommentButton">
        <button (click)="showCommentForm = true" *ngIf="!showCommentForm">Ajouter ma
            note/commentaire</button>
    </div>
    <div *ngIf="showCommentForm">
        <button (click)="showCommentForm = false">cacher</button>
        <br>
        <div class="field">
            <label class="label">Commentaire</label>
            <form action="post" [formGroup]="noteForm" (ngSubmit)="onSubmit(lesFilms.id)">
                <div class="control">
                    <textarea class="textarea" formControlName="comment"
                        placeholder="Ecrivez votre commentaire ..."></textarea>
                    <textarea class="textarea" formControlName="noteStar"
                        placeholder="Donnez votre note..."></textarea>
                    <button>Ajouter</button>
                </div>
            </form>

        </div>
    </div>

    <div *ngIf="notes.length > 0">
        <div *ngFor="let item of notes; let i = index">
            <div *ngIf="item.idFilm == lesFilms.id">
                Commentaire : {{ item.comment }} <br>
                Note : {{ item.noteStar }}/5
            </div>
        </div>
    </div>
----

==== Affichage de la liste des favoris

Lorsque l'on a ajouté des films en favoris, il s'affiche dans la page *Favoris*.



[,typescript]
----
//favoris.component.ts
lesFavoris: any=[];

ngOnInit(): void {
    this.lesFavoris = this.FilmService.getAllFavoris();

  }
----

Lorsque la page se charge, la liste des favoris est mise dans le tableau *lesFavoris*

image::src/assets/images/listesFavs.png[]

==== Supprimer un film de la liste des favoris

Nous offrons à l'utilisateur la possibilité de supprimer un film de sa liste des favoris.
Il lui suffit de cliquer sur le bouton pour que la suppression soit effective.

[,typescript]
----
//favoris.component.ts
deleteFav(id){
    console.log(id)
   this.lesFavoris = this.FilmService.deleteFavoris(id);
   this.lesFavoris = this.FilmService.getAllFavoris();
  }
----
La méthode supprime le film de la liste des favoris et reaffiche cette dernier sans le film supprimé.

[,typescript]
----
//film.service.ts
deleteFavoris(id: number) {
    this.favoris = localStorage.getItem('Favoris') ? JSON.parse(localStorage.getItem('Favoris')) : [];
    for (let i = 0; i < this.favoris.length; i++) {
      if (id == this.favoris[i].id) {
        this.favoris.splice(i, 1);
      }
    } localStorage.setItem('Favoris', JSON.stringify(this.favoris));

  }
----
Cette méthode convertit la liste des favoris du local storage en Objet JSON.

Du point de vu du html :


[,html]
----
<p class="buttons">
                        <button class="button is-danger" (click)="deleteFav(fav.id)">

                            <span>Supprimer des favoris</span>
                        </button>
                    </p>
----

image::src/assets/images/deleteFav.png[]

== Conclusion
Ce projet nous a permis de mieux comprendre le fonctionnement du framework angular. Nous avons pu découvrir de nouvelles notions et consolider celles déjà acquises. Il permet à un utilisateur de rechercher des films et d'avoir plus d'informations sur ces derniers. Il peut aussi ajoutés des films à ses favoris pour les retrouver plus facilement, ajouter une note et un commentaire à chacun. Il peut aussi décider de les supprimer de sa liste.
Nous avons apprécié travailler avec cette API très complète.

Nous n'avons pas eu de difficulté particulière lors de la réalisation de ce projet car il s'agit d'une API que Jérémie a déjà exploiter durant son stage de première année.

Malgré une période compliqué, nous sommes satisfait du résultat de notre projet. Nous aurions aussi améliorer le rendu visuel de la notion avec un système d'étoile qui serait plus simple pour l'utilisateur.

Lien du dépot distant Gitlab : https://gitlab.com/StarlingSG1/filmotec (20/12/2020)
