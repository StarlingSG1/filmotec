// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_TOKEN: '?api_key=948d11e0e6b1f34e6f583243e3af4864',
  searchMoviesbyTitle: 'https://api.themoviedb.org/3/search/movie',
  searchMovieInformationbyID: 'https://api.themoviedb.org/3/movie/',
  searchMovieCreditsbyID: '/credits',
  setLanguageToFrench: '&language=fr',
  setLanguageToFrenchQuery: '&language=fr&query=',
  setPageNumber: '&page=',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
