import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';
import  {ActivatedRoute } from '@angular/router';
import { Film } from '../film';

@Component({
  selector: 'app-favoris',
  templateUrl: './favoris.component.html',
  styleUrls: ['./favoris.component.scss']
})
export class FavorisComponent implements OnInit {

  lesFavoris: any=[];

  constructor(private FilmService: FilmService,private activatedRoute: ActivatedRoute) { }



  ngOnInit(): void {
    this.lesFavoris = this.FilmService.getAllFavoris();
    
  }

  deleteFav(id){
    console.log(id)
   this.lesFavoris = this.FilmService.deleteFavoris(id);
   this.lesFavoris = this.FilmService.getAllFavoris();
  
  }

}
