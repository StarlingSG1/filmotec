import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FilmComponent } from './film/film.component';
import { FavorisComponent } from './favoris/favoris.component';
import { HomeComponent } from './home/home.component';
import { FilmService } from './film.service';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchFilmComponent } from './search-film/search-film.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FilmComponent,
    FavorisComponent,
    HomeComponent,
    SearchFilmComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    FilmService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
