import { Component, Input, OnInit } from '@angular/core';
import  {ActivatedRoute } from '@angular/router';
import { title } from 'process';
import { FilmService, Note } from '../film.service';
import { Film } from '../film';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {
  
  lesFilms: Film ;
  crewCredits: any = [];
  castCredits: any = [];
  directorArray: any = [];
  actorsArray: any = [];
  lesFavoris: any =[];
  idFilm: number;
  duplicate: number= 0;
  showCommentForm: boolean = false;
  showCommentButton: boolean = true;
  showFavoriteButton: boolean = true;
  notes: Note[] = [];
  
  noteForm!: FormGroup;
  
  constructor(private FilmService: FilmService,private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {  }

  ngOnInit(): void {
    
     this.activatedRoute.params.subscribe(params => {
       
        const idURL: number= params['id'];
        
        //console.log('Url Id: ',idURL);
        this.FilmService.sendGetInformationbyIDRequest(idURL).subscribe((data: Object) => {
          
          
          this.FilmService.addInfoFilm(Object(data));
          this.lesFilms = this.FilmService.getAllInfoFilm();
          this.lesFavoris = this.FilmService.getAllFavoris();
          console.log(this.lesFavoris[0].id)
          for(let i = 0;  i < this.lesFavoris.length ; i++){
            if(this.lesFavoris[i].id == Object(data).id){
              this.showFavoriteButton = false;
            }
          }
          
          
        });
        this.FilmService.sendGetCreditsRequest(idURL).subscribe((data: Object) => {
          this.FilmService.addInfoFilm(data['crew']);
          this.crewCredits = this.FilmService.getAllInfoFilm();
          this.FilmService.addInfoFilm(data['cast']);
          this.castCredits = this.FilmService.getAllInfoFilm();
          for( let i = 0; i< this.crewCredits.length; i++){
            if(this.crewCredits[i].job == 'Director'){
              this.directorArray.push(this.crewCredits[i].name);
              
            }
          }
          for( let i = 0; i< 5 ; i++){
              this.actorsArray.push(this.castCredits[i].name);
          }
        });
        this.notes = this.FilmService.getNotesInLocalStoage();  
        this.noteForm=this.formBuilder.group({
          comment: new FormControl(""),
          noteStar: new FormControl("")
      });

        
        if(this.notes.length != 0){
          for(let i = 0; i < this.notes.length; i++){

            if(this.notes[i].idFilm == idURL){
              this.showCommentForm = false;
              this.showCommentButton = false;
            }
          }
  
          
        }
      });
    
     
  }
  
      onSubmit(idFilm: number){
        this.FilmService.addNoteInLocalStorage({idFilm: idFilm, comment: this.noteForm.value.comment, noteStar: this.noteForm.value.noteStar});
       
        this.showCommentForm = false;
        this.showCommentButton = false;
      }
      
  
      onClicFavoris(idURL: number){
          
          this.FilmService.sendGetInformationbyIDRequest(idURL).subscribe((data: Object) => {
            this.lesFavoris = this.FilmService.getAllFavoris();

              for(let i = 0;  i < this.lesFavoris.length ; i++){
                if(this.lesFavoris[i].title == Object(data).title){
                  this.duplicate++;
                }
              }
              if(this.duplicate == 0){
                this.FilmService.addFavoris(Object(data));
                this.showFavoriteButton = false;
              }else{
                this.showFavoriteButton = false;
              }
            
          });
      }
      
      
}
