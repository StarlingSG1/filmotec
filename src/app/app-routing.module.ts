import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmComponent } from "./film/film.component";
import { FavorisComponent } from "./favoris/favoris.component";
import { HomeComponent } from "./home/home.component";
import { SearchFilmComponent } from "./search-film/search-film.component";

const routes: Routes = [
  { path: 'film/:id', component: FilmComponent },
{ path: 'favoris', component: FavorisComponent },
{ path: '', component: HomeComponent },
{ path: 'search', component: SearchFilmComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
