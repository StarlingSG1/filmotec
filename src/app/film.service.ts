import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Film } from './film';

export type Note = {
  idFilm: number;
  comment: string;
  noteStar: number;
}
@Injectable({
  providedIn: 'root'
})
export class FilmService {

  private infosFilm: any = [];
  private infosCrew: any = [];
  private favoris: any = [];
  private notes: Note[] = [];

  constructor(private http: HttpClient) { }
  getAllInfoFilm() {
    return this.infosFilm;
  }

  addInfoFilm(Film: Film) {
    this.infosFilm = Film;
  }

  getAllFavoris() {
    this.favoris = JSON.parse(localStorage.getItem('Favoris'));
    return this.favoris
  }

  addFavoris(Film: Film) {
    this.favoris = localStorage.getItem('Favoris') ? JSON.parse(localStorage.getItem('Favoris')) : [];
    this.favoris.push(Film);
    localStorage.setItem('Favoris', JSON.stringify(this.favoris));
  }

  deleteFavoris(id: number) {
    this.favoris = localStorage.getItem('Favoris') ? JSON.parse(localStorage.getItem('Favoris')) : [];
    for (let i = 0; i < this.favoris.length; i++) {
      if (id == this.favoris[i].id) {
        this.favoris.splice(i, 1);
      }
    } localStorage.setItem('Favoris', JSON.stringify(this.favoris));

  }

  addNoteInLocalStorage(note: Note): void {
    this.notes.push(note);
    localStorage.setItem('notes', JSON.stringify(this.notes));
  }

  getNotesInLocalStoage(): Note[] {
    this.notes = JSON.parse(localStorage.getItem('notes') || '[]');
    return this.notes;
  }


  public sendGetMoviesListRequest(title: string, numPage: number) {
    return this.http.get(environment.searchMoviesbyTitle + environment.API_TOKEN + environment.setLanguageToFrenchQuery + title + environment.setPageNumber + numPage);
  }
  public sendGetInformationbyIDRequest(idFilm: number) {
    return this.http.get(environment.searchMovieInformationbyID + idFilm + environment.API_TOKEN + environment.setLanguageToFrench)
  }
  public sendGetCreditsRequest(idFilm: number) {
    return this.http.get(environment.searchMovieInformationbyID + idFilm + environment.searchMovieCreditsbyID + environment.API_TOKEN + environment.setLanguageToFrench)
  }

}