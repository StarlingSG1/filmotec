import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Film } from '../film';
import { NgModule } from '@angular/core';
import { FilmService } from '../film.service';

@Component({
  selector: 'app-search-film',
  templateUrl: './search-film.component.html',
  styleUrls: ['./search-film.component.scss']
})
export class SearchFilmComponent implements OnInit {
  @NgModule({
    providers: [
      FilmService,
      
    ],
  })
  identForm!: FormGroup;
  isSubmitted = false;
  numPage: number;
  nbPages: number;
  lesFilms: any =[] ;
  nbOflesFilms: number;
  favoris: any=[];

  constructor(private FilmService: FilmService) { 
    this.lesFilms = this.FilmService.getAllInfoFilm();
    this.nbOflesFilms = this.lesFilms.length;
  }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      title: new FormControl(),
      });
      this.lesFilms = [];
     
    }

    
    ident() {
      }

    recherche(){
      this.isSubmitted = true;
    this.FilmService.sendGetMoviesListRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {

      this.FilmService.addInfoFilm(data['results']);
      this.numPage = data['page'];
      this.nbPages = data['total_pages'];
      this.lesFilms = this.FilmService.getAllInfoFilm();
      this.favoris = this.FilmService.getAllFavoris();
      
    });
  }

 

  nextPage(){
    this.numPage++ ;
    this.FilmService.sendGetMoviesListRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {
      this.FilmService.addInfoFilm(data['results']);
      this.lesFilms = this.FilmService.getAllInfoFilm();
    });
    
  }

  previousPage(){
    this.numPage-- ;

    this.FilmService.sendGetMoviesListRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {
      this.FilmService.addInfoFilm(data['results']);
      
      this.lesFilms = this.FilmService.getAllInfoFilm();
    });
    
  }
}